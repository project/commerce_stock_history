<?php

/**
 * @file
 * Admin callbacks for Commerce Stock History Undo
 */

/**
 * Callback for the stock undo confirmation form.
 */
function commerce_stock_history_undo_form($form, &$form_state, CommerceStockHistory $stock_history_entry) {
  $form_state['stock_history_entry'] = $stock_history_entry;

  if ($product = commerce_product_load($stock_history_entry->product_id)) {
    $question = t('Are you sure you want to undo this stock adjustment?');

    $description = t('You will be adjusting the stock of <strong>@product</strong> by <strong>@amount</strong>.', array(
      '@product' => $product->title,
      '@amount' => $stock_history_entry->adjustment * -1,
    ));
  }
  else {
    drupal_set_message(t('This stock adjustment cannot be undone, the product no longer exists.'), 'error');
    return $form;
  }

  $destination = drupal_get_destination();
  $return_url = $destination['destination'] != current_path() ? $destination['destination'] : 'admin/commerce/products/' . $stock_history_entry->product_id . '/stock-history';
  $form = confirm_form($form, $question, $return_url, $description);

  $second_message = t('Are you SURE you want to undo this stock adjustment and adjust the stock of @product by @amount?', array(
    '@product' => $product->title,
    '@amount' => $stock_history_entry->adjustment * -1,
  ));

  $form['actions']['submit']['#attributes']['onclick'] = 'return window.confirm("' . $second_message . '");';

  return $form;
}

/**
 * Submit handler for the stock undo confirmation form.
 */
function commerce_stock_history_undo_form_submit($form, &$form_state) {
  $stock_history_entry = $form_state['stock_history_entry'];

  try {
    commerce_stock_history_undo_entry_undo($stock_history_entry);
    drupal_set_message(t('Stock history entry was successfully undone.'));
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }

  if (!drupal_get_destination()) {
    $form_state['redirect'] = 'admin/commerce/products/' . $stock_history_entry->product_id . '/stock-history';
  }
}
