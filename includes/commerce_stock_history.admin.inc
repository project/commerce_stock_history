<?php

/**
 * @file
 * Admin form and page callbacks for Commerce Stock History.
 */

/**
 * Page callback for viewing a stock history entity.
 */
function commerce_stock_history_view(CommerceStockHistory $entity) {
  return entity_view('commerce_stock_history', array($entity));
}

/**
 * Callback for the stock history settings form.
 */
function commerce_stock_history_settings($form, &$form_state) {
  $paths = commerce_stock_history_message_paths();
  $default_paths = array();

  foreach ($paths as $path => $message) {
    $default_paths[] = "$path|$message";
  }

  $description = t('You may define specific messages that should be mapped to paths/URLs on the website. When a stock change occurs on these paths, the corresponding message will be recorded.');
  $description .= '<br><br>';
  $description .= t('The formatting should be <strong>path</strong>|<strong>message</strong>');

  $form['commerce_stock_history_message_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Message paths'),
    '#default_value' => implode("\n", $default_paths),
    '#description' => $description,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the stock history settings form.
 */
function commerce_stock_history_settings_submit($form, &$form_state) {
  $path_values = $form_state['values']['commerce_stock_history_message_paths'];
  $path_values = preg_split('/(\r\n?|\n)/', $path_values);
  $paths = array();

  foreach ($path_values as $path_value) {
    $exploded = explode('|', $path_value);
    $paths[$exploded[0]] = $exploded[1];
  }

  variable_set('commerce_stock_history_message_paths', $paths);
}
