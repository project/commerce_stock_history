<?php

/**
 * Entity class representing the commerce_stock_history entity type.
 */
class CommerceStockHistory extends Entity {

  /**
   * The primary identifier for a stock history entry.
   *
   * @var int
   */
  public $id;

  /**
   * The URL that the stock was adjusted on.
   *
   * @var string
   */
  public $location;

  /**
   * The HTTP referer for the URL that the stock was adjusted on.
   *
   * @var string
   */
  public $referer;

  /**
   * A message that corresponds with the URL that the stock was adjusted on.
   *
   * @var string
   */
  public $message;

  /**
   * The ID of the product that the stuck was adjusted on.
   *
   * @var int
   */
  public $product_id;

  /**
   * The amount that the product's stock was adjusted by.
   *
   * @var int
   */
  public $adjustment;

  /**
   * A snapshot of the product's stock before the adjustment.
   */
  public $original_stock;

  /**
   * The ID of the user that made the change to the stock.
   *
   * @var int
   */
  public $uid;

  /**
   * A timestamp indicating when the entry was created.
   *
   * @var int
   */
  public $created;

  /**
   * The bundle name.
   *
   * @var string
   */
  public $type = 'commerce_stock_history';

  /**
   * {@inheritDoc}
   */
  public function __construct($values = array()) {
    return parent::__construct($values, 'commerce_stock_history');
  }

  /**
   * Change the default URI.
   */
  protected function defaultUri() {
    return array('path' => 'admin/commerce/stock-history/' . $this->identifier());
  }
}
