<?php

/**
 * @file
 * Controller class definition for the Commerce Stock History entity.
 */

class CommerceStockHistoryEntityController extends EntityAPIController {

  /**
   * @{inheritDoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    if ($account = user_load($entity->uid)) {
      $account_name = l($account->name, 'user/' . $account->uid . '/edit');
    }
    else {
      $account_name = t('Anonymous');
    }

    if ($product = commerce_product_load($entity->product_id)) {
      $product_title = l(t('@product_title (@sku)', array('@product_title' => $product->title, '@sku' => $product->sku)), 'admin/commerce/products/' . $product->product_id);
    }
    else {
      $product_title = $entity->product_id;
    }

    $rows = array();
    $row_details = array(
      t('Product') => $product_title,
      t('Date') => format_date($entity->created),
      t('Adjustment') => $entity->adjustment,
      t('Original stock') => $entity->original_stock,
      t('User') => $account_name,
      t('Hostname') => $entity->hostname,
      t('Location') => $entity->location,
      t('Message') => $entity->message,
      t('Referer') => $entity->referer,
    );

    foreach ($row_details as $title => $value) {
      $rows[] = array($title, $value);
    }

    $build['details'] = array(
      '#theme' => 'table',
      '#rows' => $rows
    );

    return $build;
  }
}
